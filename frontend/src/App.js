import React from "react";
import { makeStyles} from '@material-ui/core/styles';
import MetaTags from "react-meta-tags";
import HeaderContainer from "./containers/HeaderContainer";
import Button from '@material-ui/core/Button';
import Results from "./containers/Results/Results";
import LeftSide from "./containers/LeftSide";
import RightSide from "./containers/RightSide";
import firebase from "firebase";
import $ from 'jquery'; 
import "./app.css";

const firebaseConfig = {
  apiKey: "AIzaSyDeXQgdeeI1hMWSMkiUczF1NgnKWmx5SB8",
  authDomain: "videoanalyzer-9ceca.firebaseapp.com",
  databaseURL: "https://videoanalyzer-9ceca.firebaseio.com",
  projectId: "videoanalyzer-9ceca",
  storageBucket: "videoanalyzer-9ceca.appspot.com",
  messagingSenderId: "415685270181",
  appId: "1:415685270181:web:e8310897def0b405ef0b4c"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


class App extends React.Component {
  constructor(props) {
    super(props);

    this.getFileInput = this.getFileInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.sendRequestAPI = this.sendRequestAPI.bind(this);
    this.handleObjects = this.handleObjects.bind(this);
    this.updateVerAnalisis = this.updateVerAnalisis.bind(this);
    this.logoutHandler = this.logoutHandler.bind(this);
    this.volverHandler = this.volverHandler.bind(this);
    this.loadStrategys = this.loadStrategys.bind(this);

      //var apiBaseUrl = "https://videoanalyzertfg.herokuapp.com";
      var apiBaseUrl = "http://localhost:8080";
    

    var loginTime = localStorage.getItem("loginTime");
    var expiration = localStorage.getItem("expirationDuration");
    var currentTime = new Date().getTime();

    var hasExpired = currentTime - loginTime > expiration;
    var token = localStorage.getItem("tokenJWT");




    if(token != null && !hasExpired){
      this.state = {
        apiBaseURL: apiBaseUrl,
        authenticated: true,
        tokenJWT: token,
        analyzed: false,
        loading: false,
        userName: localStorage.getItem("userName")
      };
      this.handleAnalysisHistoric();
    }else{
      this.state = {
        apiBaseURL: apiBaseUrl,
        authenticated: false,
        tokenJWT: '',
        analyzed: false,
        loading: false,
        strategys: ''
      };
    }
  }

  getFileInput(e) {
    this.setState({
      fileInput: e.target.files[0]
    });
  }

  sendRequestAPI() {
    var uri = this.state.apiBaseURL + '/analysis/' + this.state.userName + "/" + $("#comboStrategies").val();
    var myHeaders = new Headers({
      'Authorization': this.state.tokenJWT
    });
    const formData= new FormData();
    var fileUri;
    var keys1;
    var keys2;

    formData.append('fileForm', this.state.fileInput);

    this.setState({
      loading: true
    });
      fetch(uri, {
      method: 'POST',
      headers: myHeaders,
      body: formData,
    })
    .then((response) => response.json())
    .then(data =>{
      keys1 = Object.keys(data);
      keys2 = Object.keys(data[keys1[0]]);
      fileUri = data[keys1[0]][keys2[0]].fileUri;
      this.setState({
        fileURI: fileUri,
        dataMap: data,
        loading: false,
        analyzed: true
      });
    } 
    );
    
  }

  isStrategySelected(){
    if(!isNaN($("#comboStrategies").val())){
      return true;
    }else{
      return false;
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    if(this.isStrategySelected()){
      this.setState({
        loading: true
      });
      this.sendRequestAPI();
    }else{
      alert("Seleccione una estrategia.");
      this.loadStrategys();
    }

  }


  handleLogin(e){
    e.preventDefault();

    fetch(this.state.apiBaseURL + '/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        userName: document.getElementById("userName").value,
        password:  document.getElementById("password").value
      })
    })
    .then((response) => { 
      if(response.status !== 401){
        const loginTime = new Date().getTime();
        const expirationDuration = 1000 * 60 * 30; // 30 min

        localStorage.setItem("loginTime", loginTime);
        localStorage.setItem("expirationDuration", expirationDuration);
        localStorage.setItem("tokenJWT", response.headers.get('authorization'));
        localStorage.setItem("userName", document.getElementById("userName").value);
        this.setState({
          authenticated: true,
          tokenJWT: response.headers.get('authorization'),
          userName: document.getElementById("userName").value
        });
        this.loadStrategys();
        this.handleAnalysisHistoric();
      }else{
        alert("Usuario no encontrado");
      }
    });

  }

  handleAnalysisHistoric(){

    var myHeaders = new Headers({
      'Authorization': this.state.tokenJWT
    });

    fetch(this.state.apiBaseURL + '/analysis/' + this.state.userName + '/historic', {
      method: 'GET',
      headers: myHeaders
    })
    .then((response) => response.json())
    .then((data) =>{
      this.setState({
        historic: data
      });
    });
  }

  handleObjects(objs){
    this.setState({
      objects: objs
    });
  }

  updateVerAnalisis(data){
    var keys1 = Object.keys(data);
    var keys2 = Object.keys(data[keys1[0]]);
    var fileUri = data[keys1[0]][keys2[0]].fileUri;
    this.setState({
      loading: true
    });

    this.setState({
      fileURI: fileUri,
      dataMap: data,
      loading: false,
      analyzed: true
    });
  }

  logoutHandler(){
    localStorage.removeItem("tokenJWT");
    localStorage.removeItem("userName");
    this.setState({
      authenticated: false,
      tokenJWT: '',
      userName: '',
      loading: false,
    });
  }


  loadStrategys(){
    var myHeaders = new Headers({
      'Authorization': this.state.tokenJWT
    });

    fetch(this.state.apiBaseURL + '/analysis/strategies', {
      method: 'GET',
      headers: myHeaders,
    })
    .then((response) => response.json())
    .then((data) => {
      var options = data.map((obj, index) =>{
        return (<option value={obj.id}>{obj.nombre}</option>);
      });
      this.setState({
        strategys: options,
      });
    });
    
  }

  volverHandler(){
    this.setState({
      analyzed: false
    });
    this.handleAnalysisHistoric();
    this.loadStrategys();
  }

  render() {
    var mainContent;
    var rightSide;
    const useStyles = makeStyles(theme => ({
      ButtonBase: {
          display: 'inline-block',
          position: 'absolute'
      },
  }));

    if (this.state.analyzed) {
      rightSide = <Results className="col-3" dataMap={this.state.dataMap} fileURL={this.state.fileURI} objHandler={this.handleObjects}/>;
    } else if(this.state.loading){
      rightSide = <div className="loader"></div>;
    }else{
      rightSide = <RightSide className="col-9" action={this.getFileInput} />;
    }
    
    
    if(this.state.authenticated){
      mainContent = (
        <form className="fileForm" encType="multipart/form-data" onSubmit={this.handleSubmit.bind(this)}>
          <LeftSide className="col-3" historic={this.state.historic} objects={this.state.objects} analyzed={this.state.analyzed} 
            updateFunc={this.updateVerAnalisis} apiBaseURL={this.state.apiBaseURL} userName={this.state.userName} tokenJWT={this.state.tokenJWT}
            logoutHandler={this.logoutHandler} volverHandler={this.volverHandler} strategys={this.state.strategys}/>
          {rightSide}
        </form>
      );
    }else{
      mainContent = (
        <form className="loginForm" onSubmit={this.handleLogin.bind(this)}>
            <label htmlFor="userName">Nombre de usuario: </label>
            <input type="text" id="userName" autoComplete="userName"></input>
            <label htmlFor="password">Contraseña: </label>
            <input type="password" id="password" name="password" autoComplete="current-password"></input>
            <Button className={useStyles.root} variant="contained" color="primary" type="submit">
                        Iniciar Sesion
            </Button>
        </form>
      );
    }

    return (
      <div className="main">
        <MetaTags>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          ></meta>
        </MetaTags>
        <HeaderContainer />
          <div className="row">
            {mainContent}
          </div>
      </div>
    );
  }
}

export default App;
