import React from 'react';
import Button from '@material-ui/core/Button';
import './rightSide.css';

class RightSide extends React.Component {

    constructor(props){
        super(props);
        
        this.fileInputHandler = this.fileInputHandler.bind(this);
        this.inputChange = this.inputChange.bind(this);

        this.state = {
            pValue: "Arrastre o seleccione un archivo para analizar",
        };
    }


    fileInputHandler(event){
        if( event.target.files[0].name != null){
            this.setState({
                pValue: event.target.files[0].name,
            });
        }else{
            this.setState({
                pValue: "Arrastre o seleccione un archivo para analizar",
            });
        }
    }

    inputChange(event){
        this.fileInputHandler(event);
        this.props.action(event);
    }

    render(){
        return(
            <div className="rightSideDiv">
                <input type="file" className="fileInput" id="fileInput"  name="file" 
                            onChange={this.inputChange} 
                             required></input>
                <p className="selectLabel">{this.state.pValue}</p>  
                <Button className="enviarButton" variant="contained" color="primary" type="submit">
                        Enviar
                </Button>
             </div>
        );
    }
}

export default RightSide;