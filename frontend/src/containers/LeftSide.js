import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import './leftSide.css';

const useStyles = makeStyles(theme => ({
    root: {
        border: '2px solid grey',
        borderRadius: 5,
        width: '100%',
        paddingRight: '3%',
        backgroundColor: theme.palette.background.paper,
        position: 'relative',
        overflow: 'auto',
        maxHeight: 300,
    },
}));



function handleVerAnalisis(selected, props){
    var myHeaders = new Headers({
        'Authorization': props.tokenJWT
      });
    fetch(props.apiBaseURL + '/analysis/' + props.userName + '/historic/' + selected.id, {
        method: 'GET',
        headers: myHeaders
      })
      .then((response) => response.json())
      .then((data) => {
        props.updateFunc(data);
      });
 
}



export default function SelectedListItem(props) {

    const classes = useStyles();
    const [selectedIndex, setSelectedIndex] = React.useState(1);
    
    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    }

    var objectNameMap = {};
    var objectList;
    var listVar;
    var user;


    if(props.analyzed){
        if(props.objects != null){
            props.objects.map((obj, key) => {
                if(obj.name in objectNameMap){
                    objectNameMap[obj.name].count++;
                }else{
                    objectNameMap[obj.name] = {
                        count: 1,
                        color: obj.color
                    };
                }
            });
            objectList = Object.keys(objectNameMap).map((key) => {
                return(
                    <ListItem
                        key={key}
                        button
                        selected={selectedIndex === key}
                        onClick = {event => handleListItemClick(event,key)}>
                        <ListItemText key={key} primary={key + "(" + objectNameMap[key].count + ")"}
                            style={{ color: objectNameMap[key].color }} />
                    </ListItem>
                );
            });
        }

        listVar = (
            <div>
                <Button variant="contained" onClick={props.volverHandler}  color="secondary">
                    Volver
                </Button>
                <p>Objetos detectados:</p>
                        <List className={classes.root} component="nav" aria-label="main mailbox folders">
                            {objectList}
                        </List>
            </div>
        );    
    }else{
        if(props.historic != null){
            objectList = props.historic.map((obj, key) =>
            <ListItem
                key = {obj.id}
                button
                selected = {setSelectedIndex === obj.id}
                onClick = {event => handleListItemClick(event,obj)}>

                    <ListItemText key={obj.id} primary={"[" + obj.analysisTime + "] - " + obj.fileName} />
                    <Button className="verAnalisis" variant="contained" color="primary" onClick={() => handleVerAnalisis(obj, props)}>
                        Ver
                    </Button>
            </ListItem>);

        user = (
            <div>
                <h2 className="usuarioTitle">Usuario: {props.userName}</h2>
                <Button variant="contained" onClick={props.logoutHandler}  className="cerrarSesion" color="secondary">
                    Cerrar Sesión
                </Button>
                <p className="selectLabel">Seleccione una estrategia de análisis:</p>
                <select id="comboStrategies" className="comboStrategies">
                    <option defaultValue>Seleccione estrategia</option>
                    {props.strategys}
                </select>
            </div>
        );
        listVar = (
            <div>
                <p>Historial de análisis:</p>
                        <List className={classes.root} component="nav" aria-label="main mailbox folders">
                            {objectList}
                        </List>
            </div>
        );   
        }
    }
    
        return(
            <div className="leftSideDiv">
                {user}
                {listVar}
            </div>
        );
}