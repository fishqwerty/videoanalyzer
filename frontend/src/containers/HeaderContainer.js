import React from 'react';
import logo from './img/logo.png';
import './headerContainer.css';

class HeaderContainer extends React.Component{
    render(){
        return (
            <header className="header">
                <img className="logo" src={logo} alt="LOGO"></img>
                <h1 className="title">Video Analyzer</h1>
                <hr className="rounded"></hr>
            </header>
        );
    }
}

export default HeaderContainer;