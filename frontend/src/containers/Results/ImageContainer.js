import React from 'react';
import './imageContainer.css';

class ImageContainer extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {dimensions: {},
                        objectDivs: [],
                        stateSecond: -1 };
        this.onImgLoad = this.onImgLoad.bind(this);
        this.handlePlay = this.handlePlay.bind(this);
        this.handleEnd = this.handleEnd.bind(this);
    }

    onImgLoad() {
        var video = document.getElementById("videoContainer");
        this.setState({dimensions:{height:video.offsetHeight,
                                   width:video.offsetWidth}});
        video.play();
    }

    
    handlePlay(){
        let style;
        var videoData = document.getElementById("videoContainer");

       if(this.props.dataMap != null){
           var data = this.props.dataMap;
           let imageWidth = this.state.dimensions.width;
           let imageHeight = this.state.dimensions.height;
           
           let actualSecond = Math.floor(videoData.currentTime);
           let tempArray  = [];
            if(actualSecond !== this.stateSecond && data[actualSecond] !== undefined){
                Object.keys(data[actualSecond]).map(keyInner => {
                        let normVertex = data[actualSecond][keyInner].vertices;
                        let color = data[actualSecond][keyInner].color;
                        let vertex = normVertex.map(function(vertex){
                            return {
                                x: vertex.x*imageWidth,
                                y: vertex.y*imageHeight
                            };
                        })

                        style = {
                            position: "absolute",
                            left: vertex[0].x + "px",
                            top: vertex[0].y + "px",
                            shapeOutside: "polygon(" + vertex[0].x +  "px " + vertex[0].y + "px, "
                                        + vertex[1].x +  "px " + vertex[1].y + "px, "
                                        + vertex[2].x +  "px " + vertex[2].y + "px, "
                                        + vertex[3].x +  "px " + vertex[3].y + "px)",
                            width: (vertex[1].x - vertex[0].x) + "px",
                            height: (vertex[3].y - vertex[0].y)+ "px",
                            float: "left",
                            border: "solid",
                            borderWidth: "3px",
                            borderColor: color
                        }
                    
                        tempArray = tempArray.concat((<div style={style} key={data[actualSecond][keyInner].id}></div>));
                });
                this.props.objHandler(data[actualSecond]);
                this.setState({
                    objectDivs: tempArray,
                    stateSecond: actualSecond
                });
            }
       }
    }

    handleEnd(){
        var videoData = document.getElementById("videoContainer");
        videoData.currentTime = 0;
        this.setState({
            stateSecond: -1
        });
        videoData.play();
    }

    render(){
        return (
            <div className="image-container">
                    <video id="videoContainer" muted src={this.props.fileURL}  onLoadedMetadata={this.onImgLoad}
                                                                        onTimeUpdate={this.handlePlay} onEnded={this.handleEnd} controls>
                    </video>
                <div id="squaresDiv"></div>
                {this.state.objectDivs}
            </div>
        );
    }
}

export default ImageContainer;