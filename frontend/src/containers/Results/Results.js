import React from 'react';
import ImageContainer from './ImageContainer';
import './results.css';

class Results extends React.Component {
    render() {
            return(
                <div className="results">
                    <ImageContainer dataMap = {this.props.dataMap} fileURL = {this.props.fileURL} objHandler={this.props.objHandler} /*objects = {this.props.objects} image={this.props.image}*/ />
                </div>
            );
    }
}

export default Results;