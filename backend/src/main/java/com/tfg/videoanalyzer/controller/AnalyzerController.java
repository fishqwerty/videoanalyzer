package com.tfg.videoanalyzer.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.tfg.videoanalyzer.entity.Analysis;
import com.tfg.videoanalyzer.entity.DetectedObject;
import com.tfg.videoanalyzer.entity.Strategy;
import com.tfg.videoanalyzer.entity.UserApp;
import com.tfg.videoanalyzer.service.AnalysisService;
import com.tfg.videoanalyzer.service.StrategyService;
import com.tfg.videoanalyzer.service.UserAppService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("analysis")
@Api(
        tags = "VideoAnalyzer API",
        description = "API para el análisis inteligente de vídeos",
        produces = "application/json",
        consumes = "application/json"
)
public class AnalyzerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AnalyzerController.class);
	
	
	// Servicios de cada entidad
	@Autowired
	private AnalysisService analysisService;
	@Autowired
	private UserAppService userService;
	@Autowired
	private StrategyService strategyService;
	@Autowired
	ResourceLoader resourceLoader;

	// Método inicial del controlador
	@PostConstruct
	public void init() {
		try {
			// Inicializamos Firebase con las credenciales de google y 
			// referenciamos a nuestra aplicación.
			
			InputStream loadedServiceAccount = resourceLoader.getResource("classpath:cloud-vision-credentials.json").getInputStream();
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(loadedServiceAccount))
					.setDatabaseUrl("https://videoanalyzer-9ceca.firebaseio.com").build();
			
			boolean hasBeenInitialized=false;
			List<FirebaseApp> firebaseApps = FirebaseApp.getApps();
			for(FirebaseApp app : firebaseApps){
			    if(app.getName().equals(FirebaseApp.DEFAULT_APP_NAME)){
			        hasBeenInitialized=true;
			    }
			}

			if(!hasBeenInitialized)
				FirebaseApp.initializeApp(options);
			
		} catch (FileNotFoundException e) {
			LOGGER.info("Error carga de credenciales Firebase. -> FileController");
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.info("Error acceso al storage de Firebase. -> FileController");
			e.printStackTrace();
		}
	}

	
	// Endpoint para el análisis de vídeo
	 @ApiOperation("Analizar vídeo.")
	 @ApiResponses({
	      @ApiResponse(code = 200, message = "El vídeo se ha analizado correctamente", response = java.util.Map.class),
	      @ApiResponse(code = 401, message = "El usuario que intenta hacer la petición no tiene autorización para realizarla."),
	      @ApiResponse(code = 403, message = "El usuario que intenta hacer la petición no tiene permiso para realizarla.")
	  })
	@PreAuthorize("isAuthenticated()")
	@PostMapping(path = "/{userName}/{strategyId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Map<Long, List<DetectedObject>>> uploadFile(@ApiIgnore Authentication aut, @PathVariable("userName") String userName,
			@PathVariable("strategyId") Long strategyId, @RequestBody MultipartFile fileForm) {
		String tokenName = (String) aut.getPrincipal();	
		
		// Comprobamos que el nombre de usuario del JWT coincide con el que hace la petición.
		if(tokenName.equals(userName)) {
			// Inicializamos la estructura que contendrá los resultados del análisis.
			Map<Long, List<DetectedObject>> objects = new HashMap<>();
			UserApp user = userService.findByUserName(userName);

			try {
				if(strategyService.existsById(strategyId)) {
					// Convertimos el archivo que llega en la petición a bytes.
					byte[] data = fileForm.getBytes();
					
					// Subimos el archivo a Firebase Storage para almacenarlo.
					Storage storage = StorageOptions.newBuilder().setProjectId("videoanalyzer-9ceca").build().getService();
					Bucket bucket = storage.get("videoanalyze2");
					bucket.toBuilder().setVersioningEnabled(true).build().update();
					BlobId blobId = BlobId.of("videoanalyze2", fileForm.getOriginalFilename());
					BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(fileForm.getContentType()).build();
					Blob blob = storage.create(blobInfo, fileForm.getBytes());

					// Creamos el objeto análisis para almacenar la información del mismo como, por ejemplo,
					// la estrategia a seguir.
					Analysis file = new Analysis(user, fileForm.getOriginalFilename(), blob.getMediaLink(),
							strategyService.findById(strategyId).get());
					
					// Lo registramos en base de datos
					file = analysisService.save(file);
					// Llamamos a la función de análisis
					objects = analysisService.extractFrames(file, data);
					// Asociamos el análisis al usuario y actualizamos en base de datos.
					user.getAnalysis().add(file);
					user = userService.save(user);
				}else {
					return ResponseEntity.notFound().build();
				}
			} catch (IOException e) {
				LOGGER.info("Error acceso al storage de Firebase. -> FileController");
				e.printStackTrace();
			}
			// Se devuelve un 200 OK con los resultados del análisis
			return ResponseEntity.ok(objects);
		}else {
			// En caso de que otra persona intente acceder al endpoint se le devuelve un 403.
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
		
		
	}

	 @ApiOperation("Obtener histórico.")
	 @ApiResponses({
	      @ApiResponse(code = 200, message = "El histórico se ha obtenido correctamente", response = Analysis.class),
	      @ApiResponse(code = 401, message = "El usuario que intenta hacer la petición no tiene autorización para realizarla."),
	      @ApiResponse(code = 403, message = "El usuario que intenta hacer la petición no tiene permiso para realizarla.")
	  })
	@PreAuthorize("isAuthenticated()")
	@GetMapping(path = "/{userName}/historic", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Analysis>> getHistoric(@ApiIgnore Authentication aut, @PathVariable("userName") String userName) {
		String tokenName = (String) aut.getPrincipal();
		
		if(tokenName.equals(userName)) {
			List<Analysis> analysisHistoric = new ArrayList<>();
			UserApp user = userService.findByUserName(userName);

			if (user != null) {
				// Obtiene todos los análisis realizados por un usuario
				analysisHistoric = analysisService.findByUser(user);

				return ResponseEntity.ok(analysisHistoric);
			} else {
				return ResponseEntity.notFound().build();
			}
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
		

	}
	
	 @ApiOperation("Obtener resultado de un histórico.")
	 @ApiResponses({
	      @ApiResponse(code = 200, message = "El resultado del histórico se ha obtenido correctamente", response = java.util.Map.class),
	      @ApiResponse(code = 401, message = "El usuario que intenta hacer la petición no tiene autorización para realizarla."),
	      @ApiResponse(code = 404, message = "No se ha encontrado el histórico solicitado."),
	      @ApiResponse(code = 403, message = "El usuario que intenta hacer la petición no tiene permiso para realizarla.")
	  })
	@PreAuthorize("isAuthenticated()")
	@GetMapping(path = "/{userName}/historic/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Map<Long, List<DetectedObject>>> getHistoricResults(@ApiIgnore Authentication aut, @PathVariable("userName") String userName,
			@PathVariable("id") Long id) {
		
		String tokenName = (String) aut.getPrincipal();
		
		if(tokenName.equals(userName)) {
			Map<Long, List<DetectedObject>> result = new HashMap<>();
			Analysis analysis;
			UserApp user = userService.findByUserName(userName);

			if (user != null) {
				// Obtenemos el análisis que se quiere visualizar
				analysis = analysisService.findById(id);
				if(analysis != null) {
					// Obtenemos los objetos detectados en el análisis
					List<DetectedObject> objects = analysis.getObjects();

					// Ahora debemos construir el objeto de resultados en función
					// de a que frame pertenencen los objetos
					long frame = -1;
					List<DetectedObject> frameObjects = new ArrayList<>();

					for (DetectedObject analysisObject : objects) {
						if (frame == analysisObject.getFrame()) {
							frameObjects.add(analysisObject);
						} else {
							if (!frameObjects.isEmpty()) {
								result.put(frame, frameObjects);
							}
							frameObjects = new ArrayList<>();
							frame = analysisObject.getFrame();
							frameObjects.add(analysisObject);
						}
					}
				}else {
					return ResponseEntity.notFound().build();
				}
				return ResponseEntity.ok(result);
			} else {
				return ResponseEntity.notFound().build();
			}
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}

	}
	
	 @ApiOperation("Obtener estrategias disponibles.")
	 @ApiResponses({
	      @ApiResponse(code = 200, message = "Las estrategias disponibles se han obtenido correctamente", response = Strategy.class),
	      @ApiResponse(code = 401, message = "El usuario que intenta hacer la petición no tiene autorización para realizarla.")
	  })
	@PreAuthorize("isAuthenticated()")
	@GetMapping(path = "/strategies", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Strategy>> getStrategies(@ApiIgnore Authentication aut) {
		List<Strategy> estrategias = new ArrayList<>();
		
		// Se obtienen todas las estrategias de análisis disponibles.
		estrategias = strategyService.findAll();

		return ResponseEntity.ok(estrategias);
	}

}
