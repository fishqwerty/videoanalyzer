package com.tfg.videoanalyzer.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.tfg.videoanalyzer.entity.UserApp;
import com.tfg.videoanalyzer.service.UserAppService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("login")
@Api(
        tags = "Login API",
        description = "API para la gestión de usuarios",
        produces = "application/json",
        consumes = "application/json"
)
public class LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private UserAppService userService;

	 @ApiOperation("Iniciar sesión.")
	 @ApiResponses({
	      @ApiResponse(code = 200, message = "Sesión iniciada correctamente.", response = UserApp.class),
	      @ApiResponse(code = 404, message = "El usuario no existe o las credenciales son incorrectas.")
	  })
	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<UserApp> login(@RequestBody UserApp user) {

		UserApp foundUser = userService.findByUserName(user.getUserName());
		
		
		if(foundUser !=  null && foundUser.getPassword().equals(user.getPassword())) {
			return ResponseEntity.ok(foundUser);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	

}
