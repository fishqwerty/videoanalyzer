package com.tfg.videoanalyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoanalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideoanalyzerApplication.class, args);
	}
	
}
