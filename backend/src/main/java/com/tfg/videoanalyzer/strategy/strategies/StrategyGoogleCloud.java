package com.tfg.videoanalyzer.strategy.strategies;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.cloud.vision.v1.LocalizedObjectAnnotation;
import com.google.cloud.vision.v1.NormalizedVertex;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.protobuf.ByteString;
import com.tfg.videoanalyzer.entity.Analysis;
import com.tfg.videoanalyzer.entity.DetectedObject;
import com.tfg.videoanalyzer.entity.Vertex;
import com.tfg.videoanalyzer.service.AnalysisService;
import com.tfg.videoanalyzer.service.DetectedObjectService;
import com.tfg.videoanalyzer.service.VertexService;
import com.tfg.videoanalyzer.strategy.StrategyTemplate;

@Component("StrategyGoogleCloud")
public class StrategyGoogleCloud extends StrategyTemplate {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StrategyGoogleCloud.class);
	
	// Generador aleatorio para los colores de los objetos.
	private Random rdmObj;
	
	@Autowired
	private AnalysisService analysisService;
	@Autowired
	private DetectedObjectService detectedObjectService;
	@Autowired
	private VertexService vertexService;
	
	public StrategyGoogleCloud() {
		// Inicializamos el objeto random con una semilla.
		this.rdmObj = new Random(20);
	}
	

	// Método para la generación de un color aleatorio
	private String generateRandomColor() {
		int rand_num = rdmObj.nextInt(0xffffff + 1);
		// format it as hexadecimal string and print
		String colorCode = String.format("#%06x", rand_num);
		return colorCode;
	}

	// Método para el análisis de un frame
	public Map<Long, List<DetectedObject>> analyzeFrame(Analysis file, Map<Long, ByteString> images, long initFrame,
			Map<String, String> objectColors) {
		Map<Long, List<DetectedObject>> objects = new HashMap<>();
		List<DetectedObject> frameObjects = new ArrayList<>();
		List<DetectedObject> analysisObjects = new ArrayList<>();
		// Instanciamos un cliente de Google Cloud Vision API
		try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {
			// Construimos la petición de análisis
			List<AnnotateImageRequest> requests = new ArrayList<>();
			for (Map.Entry<Long, ByteString> entry : images.entrySet()) {
				Image img = Image.newBuilder().setContent(entry.getValue()).build();
				// Aquí indicamos que queremos realizar una detección de objectos. Estas serían
				// estrategias propias de Google Cloud Vision
				Feature feat = Feature.newBuilder().setType(Type.OBJECT_LOCALIZATION).build();
				AnnotateImageRequest request = AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img)
						.build();
				// Construimos el batch con los 16 frames que vamos a enviar a analizar
				requests.add(request);
			}

			// Realizamos la petición a Google Cloud Vision API
			BatchAnnotateImagesResponse response = vision.batchAnnotateImages(requests);
			List<AnnotateImageResponse> responses = response.getResponsesList();

			// Obtenemos el frame en el que nos encontramos actualmente
			long frame = initFrame;
			for (AnnotateImageResponse res : responses) {
				frameObjects = new ArrayList<>();
				for (LocalizedObjectAnnotation entity : res.getLocalizedObjectAnnotationsList()) {
					// Generamos un color único para cada objeto detectado
					if (!objectColors.containsKey(entity.getName())) {
						String colorCode = generateRandomColor();
						objectColors.put(entity.getName(), colorCode);
					}
					// Inicializamos la entidad para almacenar el objeto detectado.
					DetectedObject object = new DetectedObject(entity.getName(), entity.getScore(), file.getFile(),
							objectColors.get(entity.getName()));
					ArrayList<Vertex> vertices = new ArrayList<>();
					object.setFrame(frame);
					object = detectedObjectService.save(object);
					// Obtenemos los vértices normalizados del objeto
					List<NormalizedVertex> listVertex = entity.getBoundingPoly().getNormalizedVerticesList();
					for (NormalizedVertex vertex : listVertex) {
						Vertex vertice = new Vertex(vertex.getX(), vertex.getY());
						vertices.add(vertice);
						vertexService.save(vertice);
					}
					object.setVertices(vertices);
					object = detectedObjectService.save(object);
					frameObjects.add(object);
					analysisObjects.add(object);
				}
				objects.put(frame, frameObjects);
				frame++;
			}
			file.getObjects().addAll(analysisObjects);
			analysisService.save(file);
		} catch (IOException e) {
			LOGGER.info("Error al analizar frame. -> FileController");
			e.printStackTrace();
		}
		return objects;
	}

	// Método encargado de la descomposición en frames del vídeo
	public Map<Long, List<DetectedObject>> extractFrames(Analysis file, byte[] fileBytes) {
		Map<Long, List<DetectedObject>> objects = new HashMap<>();
		// Almacena el color de cada objeto detectado de forma que sean únicos.
		Map<String, String> objectColors = new HashMap<>();
		try {
			// Inicializamos el Grabber para comenzar la descomposición en frames
			FFmpegFrameGrabber g = new FFmpegFrameGrabber(new ByteArrayInputStream(fileBytes));
			g.start();
			int frameCount = g.getLengthInFrames();
			Map<Long, ByteString> images = new HashMap<>();
			long initFrame = 0;
			// Obtenemos un frame por segundo
			for (int i = 0; i < frameCount; i += g.getFrameRate()) {
				if (i > 0)
					g.setFrameNumber(i);
				Frame frame = g.grabImage();
				if (frame == null)
					break;
				if (frame.image == null)
					continue;
				// Obtenemos el número de frame que, en este caso, coincide con el segundo
				// actual
				long frameNumber = g.getTimestamp() / 1000000;
				// Convertimos el frame a una imagen JPEG para mandarla a analizar
				BufferedImage image = new Java2DFrameConverter().convert(frame);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(image, "jpeg", baos);
				// Metemos cada imagen en un HahsMap para proceder a su envío
				images.put(frameNumber, ByteString.copyFrom(baos.toByteArray()));
				// Solo podemos mandar a analizar un máximo de 16 imágenes.
				if (!(images.size() > 14)) {
					// Realizamos la petición
					objects.putAll(analyzeFrame(file, images, initFrame, objectColors));
					// Borramos el HashMap para seguir recogiendo frames.
					images.clear();
					// Almacenamos el frame en el que nos hemos quedado para continuar por si
					// hubiese más frames que analizar
					initFrame = frameNumber;
				}
			}

			if (!images.isEmpty()) {
				objects.putAll(analyzeFrame(file, images, initFrame, objectColors));
				images.clear();
			}

			g.stop();
		} catch (Exception e) {
			LOGGER.info("Error al extraer los frames del video -> FileController");
			e.printStackTrace();
		}
		return objects;
	}

}
