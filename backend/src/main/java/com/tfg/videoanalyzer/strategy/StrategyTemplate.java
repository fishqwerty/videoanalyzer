package com.tfg.videoanalyzer.strategy;

import java.util.List;
import java.util.Map;

import com.google.protobuf.ByteString;
import com.tfg.videoanalyzer.entity.Analysis;
import com.tfg.videoanalyzer.entity.DetectedObject;

public abstract class StrategyTemplate {

	// Estrategia 1
	public abstract Map<Long, List<DetectedObject>> extractFrames(Analysis file, byte[] fileBytes);
	public abstract Map<Long, List<DetectedObject>> analyzeFrame(Analysis file, Map<Long, ByteString> images, long initFrame, Map<String, String> objectColors);
}
