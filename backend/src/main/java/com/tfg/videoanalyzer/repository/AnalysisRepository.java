package com.tfg.videoanalyzer.repository;

import com.tfg.videoanalyzer.entity.*;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnalysisRepository extends CrudRepository<Analysis, Long> {

	List<Analysis> findByUser(UserApp user);
}
