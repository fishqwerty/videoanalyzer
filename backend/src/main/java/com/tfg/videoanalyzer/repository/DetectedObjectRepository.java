package com.tfg.videoanalyzer.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tfg.videoanalyzer.entity.DetectedObject;
import com.tfg.videoanalyzer.entity.Analysis;

@Repository
public interface DetectedObjectRepository extends CrudRepository<DetectedObject, Long>{

	
}
