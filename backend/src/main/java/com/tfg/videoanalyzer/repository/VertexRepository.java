package com.tfg.videoanalyzer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tfg.videoanalyzer.entity.Vertex;


@Repository
public interface VertexRepository extends CrudRepository<Vertex, Long>{

}
