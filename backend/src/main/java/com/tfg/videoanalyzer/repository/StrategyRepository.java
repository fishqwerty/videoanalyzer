package com.tfg.videoanalyzer.repository;

import org.springframework.data.repository.CrudRepository;

import com.tfg.videoanalyzer.entity.Strategy;


public interface StrategyRepository extends CrudRepository<Strategy, Long>{

	Strategy findByNombre(String nombre);
}
