package com.tfg.videoanalyzer.repository;


import org.springframework.data.repository.CrudRepository;

import com.tfg.videoanalyzer.entity.UserApp;

public interface UserAppRepository extends CrudRepository<UserApp, Long>{
	
	UserApp findByUserName(String userName);
	
}
