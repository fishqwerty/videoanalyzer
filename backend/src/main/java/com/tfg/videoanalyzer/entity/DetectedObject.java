package com.tfg.videoanalyzer.entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "objetos")
public class DetectedObject {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long frame;
	private String name;
	private float precisión;
	private String color;

	private String fileUri;
	@OneToMany(cascade=CascadeType.ALL)
	private List<Vertex> vertices;

	public DetectedObject() {}
	
	public DetectedObject(String name, float confidence, String fileUri, String color) {
		this.name = name;
		this.precisión = confidence;
		this.fileUri = fileUri;
		this.vertices = new ArrayList<>();
		this.color = color;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getFileUri() {
		return fileUri;
	}

	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}

	public float getPrecisión() {
		return precisión;
	}
	public void setPrecisión(float precisión) {
		this.precisión = precisión;
	}

	public Long getFrame() {
		return frame;
	}

	public void setFrame(Long frame) {
		this.frame = frame;
	}

	public List<Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(List<Vertex> vertices) {
		this.vertices = vertices;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
}
