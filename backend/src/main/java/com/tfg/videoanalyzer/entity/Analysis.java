package com.tfg.videoanalyzer.entity;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "analytics")
public class Analysis {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private UserApp user;
	private String fileName;
	private String file;
	@JsonIgnore
	@OneToMany
	private List<DetectedObject> objects;
	@OneToOne
	private Strategy strategy;
	private String analysisTime;
	
	public Analysis() {}
	
	public Analysis(UserApp user, String fileName, String fileURI, Strategy strategy) {
		this.user = user;
		this.fileName = fileName;
		this.file = fileURI;
		this.strategy = strategy;
		this.objects = new ArrayList<>();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss ");
		this.analysisTime = format.format(new Date());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public UserApp getUser() {
		return user;
	}
	public void setUser(UserApp user) {
		this.user = user;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public String getAnalysisTime() {
		return analysisTime;
	}

	public void setAnalysisTime(String analysisTime) {
		this.analysisTime = analysisTime;
	}

	public List<DetectedObject> getObjects() {
		return objects;
	}

	public void setObjects(List<DetectedObject> objects) {
		this.objects = objects;
	}
	
	
	
}