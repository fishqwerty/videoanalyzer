package com.tfg.videoanalyzer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tfg.videoanalyzer.entity.Strategy;
import com.tfg.videoanalyzer.repository.StrategyRepository;


@Service
public class StrategyService {
	
	@Autowired
	private StrategyRepository strategyRepo;
	
	public List<Strategy> findAll() {
		return (List<Strategy>) strategyRepo.findAll(); 
	}
	
	public Strategy findByNombre(String nombre) {
		return strategyRepo.findByNombre(nombre);
	}
	
	public void save(Strategy object) {
		strategyRepo.save(object);
	}

	public Optional<Strategy> findById(Long strategyId) {	
		return strategyRepo.findById(strategyId);
	}

	public boolean existsById(Long strategyId) {
		return strategyRepo.existsById(strategyId);
	}

	
}
