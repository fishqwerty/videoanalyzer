package com.tfg.videoanalyzer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tfg.videoanalyzer.entity.DetectedObject;
import com.tfg.videoanalyzer.entity.Analysis;
import com.tfg.videoanalyzer.repository.DetectedObjectRepository;

@Service
public class DetectedObjectService {

	@Autowired
	private DetectedObjectRepository objectRepo;
	
	public List<DetectedObject> findAll() {
		return (List<DetectedObject>) objectRepo.findAll(); 
	}
	
	public DetectedObject save(DetectedObject object) {
		return objectRepo.save(object);
	}

}
