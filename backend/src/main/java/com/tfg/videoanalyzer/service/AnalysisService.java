package com.tfg.videoanalyzer.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tfg.videoanalyzer.entity.Analysis;
import com.tfg.videoanalyzer.entity.DetectedObject;
import com.tfg.videoanalyzer.entity.UserApp;
import com.tfg.videoanalyzer.repository.AnalysisRepository;
import com.tfg.videoanalyzer.strategy.StrategyTemplate;

@Service
public class AnalysisService {
	
	@Autowired
	@Qualifier("StrategyGoogleCloud")
	private StrategyTemplate strategyTemplate;

	@Autowired
	private AnalysisRepository fileRepo;

	public List<Analysis> findAll() {
		return (List<Analysis>) fileRepo.findAll(); 
	}
	
	public List<Analysis> findByUser(UserApp user) {
		return (List<Analysis>) fileRepo.findByUser(user);
	}
	
	public Analysis findById(Long id) {
		return fileRepo.findById(id).orElse(null);
	}
	
	public Analysis save(Analysis file) {
		return fileRepo.save(file);
	}
	
	public Map<Long, List<DetectedObject>> extractFrames(Analysis file, byte[] fileBytes){
		// En función de cual sea la estratregia seleccionada por el usuario, 
		// se utilizaría la implementación correspondiente de StrategyTemplate.
		switch(file.getStrategy().getId().intValue()) {
		case 0:
			return this.strategyTemplate.extractFrames(file, fileBytes);
		default:
			return this.strategyTemplate.extractFrames(file, fileBytes);
		}
	}
	
}
