package com.tfg.videoanalyzer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tfg.videoanalyzer.entity.Vertex;
import com.tfg.videoanalyzer.repository.VertexRepository;


@Service
public class VertexService {
	@Autowired
	private VertexRepository verticeRepo;
	
	public List<Vertex> findAll() {
		return (List<Vertex>) verticeRepo.findAll(); 
	}
	
	public void save(Vertex object) {
		verticeRepo.save(object);
	}

}
