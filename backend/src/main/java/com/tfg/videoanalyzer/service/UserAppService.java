package com.tfg.videoanalyzer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tfg.videoanalyzer.entity.UserApp;
import com.tfg.videoanalyzer.repository.UserAppRepository;


@Service
public class UserAppService implements UserDetailsService{
	@Autowired
	private UserAppRepository userRepo;
	
	public List<UserApp> findAll() {
		return (List<UserApp>) userRepo.findAll(); 
	}
	
	public UserApp findByUserName(String username) {
		return userRepo.findByUserName(username);
	}
	
	
	public UserApp save(UserApp object) {
		return userRepo.save(object);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserApp user = userRepo.findByUserName(username);
		
		String rol = "ROLE_USER";
        if(user == null)
            throw new UsernameNotFoundException(username);
		 List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(rol);
        return new User(user.getUserName(), user.getPassword(), true, true, true, true, authorities);
	}

}
