package com.tfg.videoanalyzer.configuration;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.tfg.videoanalyzer.filter.AuthenticationFilter;
import com.tfg.videoanalyzer.filter.AuthorizationFilter;
import com.tfg.videoanalyzer.service.UserAppService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthenticationConfig extends WebSecurityConfigurerAdapter {

    private final UserAppService userAuthDetailsService;

    private final PasswordEncoder passwordEncoder;
    
    private final RoleHierarchy roleHierarchy;

    @Autowired
    public AuthenticationConfig(UserAppService userAuthDetailsService, PasswordEncoder passwordEncoder, RoleHierarchy roleHierarchy) {
        this.userAuthDetailsService = userAuthDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.roleHierarchy = roleHierarchy;
    }
   

    // Configuramos los parámetros de seguridad
    @Override
    protected void configure(HttpSecurity http) throws Exception {
       /* http
        		.cors().and()
                // Se desactiva el filtro CSRF: Cross-Site request forgery
                .csrf().disable()
                // TODO - QUITAR FRAMEOPTIONS
                .headers().frameOptions().disable()
                .and()
                // Se necesita autenticacion para realizar peticiones
                .authorizeRequests()
                .antMatchers("/files").hasAuthority("ROLE_USER")
                .anyRequest().authenticated()
                .and()
                //Se define el filtro autenticacion y el de autorizacion
                .addFilter(new AuthorizationFilter(authenticationManager()))
                .addFilter(new AuthenticationFilter(userAuthDetailsService, authenticationManager()))
                // Se desactiva el uso de cookies
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);*/
		        http
				.cors().and()
		        // Se desactiva el filtro CSRF: Cross-Site request forgery
		        .csrf().disable()
		        // TODO - QUITAR FRAMEOPTIONS
		        .headers().frameOptions().disable()
		        .and()
		        // Se necesita autenticacion para realizar peticiones
		        .authorizeRequests()
		        .anyRequest().permitAll()
		        .and()
		        //Se define el filtro autenticacion y el de autorizacion
		        .addFilter(new AuthorizationFilter(authenticationManager()))
		        .addFilter(new AuthenticationFilter(userAuthDetailsService, authenticationManager()))
		        // Se desactiva el uso de cookies
		        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

	    // Se configura la clase que recupera los usuarios y el algoritmo de encriptado
	    @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.userDetailsService(userAuthDetailsService).passwordEncoder(passwordEncoder);
	    }
	    
	
	    @Bean
	    public CorsConfigurationSource corsConfigurationSource() {
	    CorsConfiguration configuration = new CorsConfiguration();
	    configuration.setAllowedOrigins(Arrays.asList("*"));
	    configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
	    configuration.setAllowedHeaders(
	    Arrays.asList("authorization", "content-type", "x-auth-token", "x-authorization-renew", "x-login-id"));
	    configuration
	    .setExposedHeaders(Arrays.asList("authorization", "x-authorization-renew", "x-authorization-renew"));
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    source.registerCorsConfiguration("/**", configuration);
	    return source;
	    }
	    
	    // Configuramos la clase que provee la jerarquía de roles
	    @Override
	    public void configure(WebSecurity web) {
	        DefaultWebSecurityExpressionHandler expressionHandler = new DefaultWebSecurityExpressionHandler();
	        expressionHandler.setRoleHierarchy(roleHierarchy);
	        web.expressionHandler(expressionHandler).ignoring().antMatchers("/v2/api-docs",
                    "/configuration/ui",
                    "/swagger-resources/**",
                    "/configuration/security",
                    "/swagger-ui.html",
                    "/webjars/**");;
	    }

}
