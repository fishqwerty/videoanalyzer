INSERT INTO strategies (nombre) VALUES
	('Estrategia 1');
	
INSERT INTO users (id, user_name, password) VALUES
	(1, 'user1', '$2y$12$mh.y1MflJDLQ/TIUlpzwfOi5p/kBaiuBp9DC2A.zMOXHGwpMXEeKe'),
	(2, 'user2', '$2y$12$bKSDOO7gM9AeN7IKC0qBruyR0GNIBkvwdsjsaAJVBIeK.gHbv5BhO');