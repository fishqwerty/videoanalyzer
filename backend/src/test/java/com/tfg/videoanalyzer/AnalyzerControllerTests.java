package com.tfg.videoanalyzer;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.videoanalyzer.entity.Analysis;
import com.tfg.videoanalyzer.entity.Strategy;
import com.tfg.videoanalyzer.entity.UserApp;

@SpringBootTest(classes = VideoanalyzerApplication.class, 
					webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = {"/applicationtest.properties"})
public class AnalyzerControllerTests {

	@LocalServerPort
	private int port;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void login() {
		assertNotNull(getToken("user1","user1"));
	}
	
	@Test
	public void testGetStrategies() {
		List<Object> strategies = new ArrayList<>();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getToken("user1","user1"));
		 HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<Strategy[]> response = restTemplate.exchange(createURLWithPort("/analysis/strategies"), HttpMethod.GET, request, Strategy[].class);
		Strategy[] result = response.getBody();
		strategies = Arrays.asList(result);
		assertTrue(!strategies.isEmpty());
	}
	
	@Test
	public void testAnalysis() {
		Resource resource = resourceLoader.getResource("classpath:Street-video.mp4");
		try {
			File file = resource.getFile();
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", getToken("user1","user1"));
			MockMultipartFile upload = new MockMultipartFile("fileForm", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, Files.readAllBytes(file.toPath()));
			mockMvc.perform(multipart("/analysis/user1/1")
								.file(upload)
								.headers(headers))
									.andExpect(status().is(200));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetHistoric() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getToken("user1","user1"));
		 HttpEntity<String> request = new HttpEntity<String>(headers);
		 ResponseEntity<Analysis[]> response = restTemplate.exchange(createURLWithPort("/analysis/user1/historic"), HttpMethod.GET, request, Analysis[].class);
		 
		 assertTrue(response.getStatusCode() == HttpStatus.OK);
	}
	
	@Test
	public void testGetHistoricResults() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getToken("user1","user1"));
		HttpEntity<String> request = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/analysis/user1/historic/1"), HttpMethod.GET, request, String.class);
		 
		 assertTrue(response.getStatusCode() == HttpStatus.OK);
	}
	
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    
    private String getToken(String userName, String password) {
		UserApp user = new UserApp(userName, password);
		ObjectMapper objectMapper = new ObjectMapper();
		HttpEntity<String> request;
		HttpHeaders headers = new HttpHeaders();
		try {
			request = new HttpEntity<String>(objectMapper.writeValueAsString(user));
			HttpEntity<String> response = restTemplate.exchange(createURLWithPort("/login"), HttpMethod.POST, request, String.class);
			headers = response.getHeaders();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		
		return headers.get("Authorization").get(0);
    }
}
